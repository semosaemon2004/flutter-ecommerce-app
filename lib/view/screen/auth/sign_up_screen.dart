import 'package:ecommerce_app/core/class/status_request.dart';
import 'package:ecommerce_app/core/constant/colors.dart';
import 'package:ecommerce_app/core/functions/alert_exit_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../../../controller/auth/sign_up_contoller.dart';
import '../../../core/functions/valid_input.dart';
import '../../widget/auth/custom_button_auth.dart';
import '../../widget/auth/custom_text_body_auth.dart';
import '../../widget/auth/custom_text_form_auth.dart';
import '../../widget/auth/custom_text_signup_auth.dart';
import '../../widget/auth/custom_text_title_auth.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => SignUpControllerImp());
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AppColor.backgroundColor,
        elevation: 0.0,
        title: Text(
          '17'.tr,
          style: Theme.of(context).textTheme.headline1!.copyWith(
                color: AppColor.textTitle,
                fontSize: 22,
                fontWeight: FontWeight.w400,
              ),
        ),
      ),
      body: WillPopScope(
        onWillPop: alertExitApp,
        child: GetBuilder<SignUpControllerImp>(
          builder: (controller) => controller.statusRequest ==
                  StatusRequest.loading
              ? Center(
                  child: SpinKitThreeInOut(
                    size: 50.0,
                    itemBuilder: (_, int index) {
                      return DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: index.isEven
                              ? const Color(0xFF7a9ee6)
                              : Colors.black,
                        ),
                      );
                    },
                  ),
                )
              : Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                  child: Form(
                    key: controller.formstate,
                    child: ListView(
                      children: [
                        const SizedBox(height: 10),
                        CustomTextTitleAuth(text: "10".tr),
                        const SizedBox(height: 10),
                        CustomTextBodyAuth(text: "24".tr),
                        const SizedBox(height: 30),
                        CustomTextFormAuth(
                          isNumber: false,

                          valid: (val) {
                            return validInput(val!, 3, 20, "username");
                          },
                          mycontroller: controller.username,
                          hinttext: "23".tr,
                          iconData: Icons.person_outline,
                          labeltext: "20".tr,
                          // mycontroller: ,
                        ),
                        const SizedBox(height: 10),
                        CustomTextFormAuth(
                          isNumber: false,

                          valid: (val) {
                            return validInput(val!, 3, 40, "email");
                          },
                          mycontroller: controller.email,
                          hinttext: "12".tr,
                          iconData: Icons.email_outlined,
                          labeltext: "18".tr,
                          // mycontroller: ,
                        ),
                        const SizedBox(height: 10),
                        CustomTextFormAuth(
                          isNumber: true,
                          valid: (val) {
                            return validInput(val!, 7, 11, "phone");
                          },
                          mycontroller: controller.phoneNumber,
                          hinttext: "22".tr,
                          iconData: Icons.phone_android_outlined,
                          labeltext: "21".tr,
                          // mycontroller: ,
                        ),
                        const SizedBox(height: 10),
                        GetBuilder<SignUpControllerImp>(
                          builder: (controler) => CustomTextFormAuth(
                            obscureText: controller.isshowpassword,
                            onTapIcon: () {
                              controller.showPassword();
                            },
                            isNumber: false,
                            valid: (val) {
                              return validInput(val!, 5, 30, "password");
                            },
                            mycontroller: controller.password,
                            hinttext: "13".tr,
                            iconData: Icons.lock_outline,
                            labeltext: "19".tr,
                            // mycontroller: ,
                          ),
                        ),
                        CustomButtomAuth(
                            text: "17".tr,
                            onPressed: () {
                              controller.signUp();
                            }),
                        const SizedBox(height: 30),
                        CustomTextSignUpOrSignInAuth(
                          onTap: () {
                            controller.goToSignIn();
                          },
                          textOne: "25".tr,
                          textTwo: "26".tr,
                        ),
                      ],
                    ),
                  ),
                ),
        ),
      ),
    );
  }
}
