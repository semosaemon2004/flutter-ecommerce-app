import 'package:ecommerce_app/core/constant/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../controller/auth/forget_password_contoller.dart';
import '../../../../core/functions/valid_input.dart';
import '../../../widget/auth/custom_button_auth.dart';
import '../../../widget/auth/custom_text_body_auth.dart';
import '../../../widget/auth/custom_text_form_auth.dart';
import '../../../widget/auth/custom_text_title_auth.dart';

class ForgetPasswordScreen extends StatelessWidget {
  const ForgetPasswordScreen({super.key});

  @override
  Widget build(BuildContext context) {
    ForgetPasswordControllerImp controller =
        Get.put(ForgetPasswordControllerImp());
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AppColor.backgroundColor,
        elevation: 0.0,
        title: Text(
          '14'.tr,
          style: Theme.of(context).textTheme.headline1!.copyWith(
                color: AppColor.textTitle,
                fontSize: 22,
                fontWeight: FontWeight.w400,
              ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
        child: Form(
          key: controller.formstate,
          child: ListView(
            children: [
              const SizedBox(height: 10),
              CustomTextTitleAuth(text: "27".tr),
              const SizedBox(height: 10),
              CustomTextBodyAuth(text: "29".tr),
              const SizedBox(height: 80),
              CustomTextFormAuth(
                isNumber: false,
                valid: (val) {
                  return validInput(val!, 5, 100, "email");
                },
                mycontroller: controller.email,
                hinttext: "12".tr,
                iconData: Icons.email_outlined,
                labeltext: "18".tr,
                // mycontroller: ,
              ),
              const SizedBox(height: 80),
              CustomButtomAuth(
                  text: "30".tr,
                  onPressed: () {
                    controller.goToVerfiyCode();
                  }),
              const SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}
