import 'package:ecommerce_app/core/constant/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../controller/auth/reset_password_controller.dart';
import '../../../../core/functions/valid_input.dart';
import '../../../widget/auth/custom_button_auth.dart';
import '../../../widget/auth/custom_text_body_auth.dart';
import '../../../widget/auth/custom_text_form_auth.dart';
import '../../../widget/auth/custom_text_title_auth.dart';

class ResetPasswordScreen extends StatelessWidget {
  const ResetPasswordScreen({super.key});

  @override
  Widget build(BuildContext context) {
    ResetPasswordControllerImp controller =
        Get.put(ResetPasswordControllerImp());
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AppColor.backgroundColor,
        elevation: 0.0,
        title: Text(
          "Reset Password",
          style: Theme.of(context).textTheme.headline1!.copyWith(
                color: AppColor.textTitle,
                fontSize: 22,
                fontWeight: FontWeight.w400,
              ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
        child: Form(
          key: controller.formstate,
          child: ListView(
            children: [
              const SizedBox(height: 10),
              CustomTextTitleAuth(text: "35".tr),
              const SizedBox(height: 10),
              CustomTextBodyAuth(text: "35".tr),
              const SizedBox(height: 80),
              GetBuilder<ResetPasswordControllerImp>(
                builder: (controler) => CustomTextFormAuth(
                  obscureText: controller.isshowpassword,
                  onTapIcon: () {
                    controller.showPassword();
                  },
                  isNumber: false,
                  valid: (val) {
                    return validInput(val!, 5, 30, "password");
                  },
                  mycontroller: controller.password,
                  hinttext: "13".tr,
                  iconData: Icons.lock_outline,
                  labeltext: "19".tr,
                  // mycontroller: ,
                ),
              ),
              const SizedBox(height: 30),
              GetBuilder<ResetPasswordControllerImp>(
                builder: (controller) => CustomTextFormAuth(
                  obscureText: controller.isshowpassword,
                  onTapIcon: () {
                    controller.showPassword();
                  },
                  isNumber: false,
                  valid: (val) {
                    return validInput(val!, 3, 40, "password");
                  },
                  mycontroller: controller.password,
                  hinttext: "Re" + " " + "13".tr,
                  iconData: Icons.lock_outline,
                  labeltext: "19".tr,
                  // mycontroller: ,
                ),
              ),
              const SizedBox(height: 50),
              CustomButtomAuth(
                text: "33".tr,
                onPressed: () {
                  controller.goToSccessResetPassword();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
