import 'package:ecommerce_app/data/model/cart_model.dart';
import 'package:ecommerce_app/view/widget/cart/check_out_card.dart';
import 'package:flutter/material.dart';

import '../../../core/constant/colors.dart';
import '../../widget/cart/body.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            const Text(
              "Your Cart",
              style: TextStyle(color: Colors.black),
            ),
            Text(
              "${demoCarts.length} items",
              style: Theme.of(context).textTheme.caption,
            ),
          ],
        ),
      ),
      body: Body(),
      bottomNavigationBar: CheckoutCard(),
    );
  }
}
