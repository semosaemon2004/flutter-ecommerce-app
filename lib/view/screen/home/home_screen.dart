import 'package:flutter/material.dart';

import '../../../enums.dart';
import '../../../core/constant/custom_bottom_nav_bar.dart';
import '../../widget/home/body.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
    );
  }
}