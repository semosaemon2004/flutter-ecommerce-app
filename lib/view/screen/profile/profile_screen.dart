import 'package:flutter/material.dart';

import '../../../core/constant/colors.dart';
import '../../../enums.dart';
import '../../../core/constant/custom_bottom_nav_bar.dart';
import '../../widget/profile/body.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        centerTitle: true,
        backgroundColor: AppColor.backgroundColor,
        elevation: 0.0,
        title: Text(
          "Profile",
          style: Theme.of(context).textTheme.headline1!.copyWith(
                color: AppColor.textTitle,
                fontSize: 22,
                fontWeight: FontWeight.w400,
              ),
        ),
      ),
      body: Body(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.profile),
    );
  }
}