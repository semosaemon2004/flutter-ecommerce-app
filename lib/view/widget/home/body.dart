import 'package:ecommerce_app/view/widget/home/categories.dart';
import 'package:ecommerce_app/view/widget/home/discount_banner.dart';
import 'package:ecommerce_app/view/widget/home/home_header.dart';
import 'package:ecommerce_app/core/constant/popular_products.dart';
import 'package:ecommerce_app/view/widget/home/special_offers.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: const [
            SizedBox(height: 20),
            HomeHeader(),
            SizedBox(height: 10),
            DiscountBanner(),
            Categories(),
            SpecialOffers(),
            SizedBox(height: 30),
            PopularProducts(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }
}
