import 'package:ecommerce_app/core/constant/icons.dart';
import 'package:ecommerce_app/view/screen/cart/cart_screen.dart';
import 'package:ecommerce_app/view/widget/home/search_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/constant/routes_name.dart';
import '../../../size_confing.dart';
import 'icon_btn_with_counter.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
         const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SearchField(),
          IconBtnWithCounter(
            svgSrc: AppIcons.cartIcon,
            press: () => Get.toNamed(AppRoute.card),
          ),
          IconBtnWithCounter(
            svgSrc: AppIcons.bell,
            numOfitem: 11,
            press: () {},
          ),
        ],
      ),
    );
  }
}
