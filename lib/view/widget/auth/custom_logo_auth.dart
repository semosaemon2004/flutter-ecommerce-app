import 'package:flutter/material.dart';

import '../../../core/constant/images.dart';

class CustomLogoAuth extends StatelessWidget {
  const CustomLogoAuth({super.key});

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 60,
      backgroundColor: Colors.red,
      child: Padding(
        padding: const EdgeInsets.all(0), // Border radius
        child: ClipOval(
          child: Image.asset(
            AppImages.logo,
          ),
        ),
      ),
    );
  }
}
