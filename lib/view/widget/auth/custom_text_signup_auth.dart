import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/constant/colors.dart';

class CustomTextSignUpOrSignInAuth extends StatelessWidget {
  final String textOne;
  final String textTwo;
  final Function() onTap;
  const CustomTextSignUpOrSignInAuth(
      {super.key,
      required this.textOne,
      required this.textTwo,
      required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(textOne),
        const SizedBox(width: 10),
        InkWell(
          onTap: onTap,
          child: Text(
            textTwo,
            style: const TextStyle(
              color: AppColor.primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
