import 'package:flutter/material.dart';

import '../../../core/constant/colors.dart';

class CustomAppBarAuth extends StatelessWidget {
  final String title;
  const CustomAppBarAuth({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      backgroundColor: AppColor.backgroundColor,
      elevation: 0.0,
      title: Text(
        title,
        style: Theme.of(context).textTheme.headline1!.copyWith(
              color: AppColor.textTitle,
              fontSize: 22,
              fontWeight: FontWeight.w400,
            ),
      ),
    );
  }
}
