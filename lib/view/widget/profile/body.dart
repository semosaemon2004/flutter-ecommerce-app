import 'package:ecommerce_app/core/constant/icons.dart';
import 'package:ecommerce_app/view/widget/profile/profile_menu.dart';
import 'package:ecommerce_app/view/widget/profile/profile_pic.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          const ProfilePic(),
          const SizedBox(height: 20),
          ProfileMenu(
            text: "My Account",
            icon: AppIcons.userIcon,
            press: () => {},
          ),
          ProfileMenu(
            text: "Notifications",
            icon: AppIcons.bell,
            press: () {},
          ),
          ProfileMenu(
            text: "Settings",
            icon: AppIcons.settings,
            press: () {},
          ),
          ProfileMenu(
            text: "Help Center",
            icon: AppIcons.questionMark,
            press: () {},
          ),
          ProfileMenu(
            text: "Log Out",
            icon: AppIcons.logOut,
            press: () {},
          ),
        ],
      ),
    );
  }
}
