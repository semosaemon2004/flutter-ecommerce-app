import 'package:ecommerce_app/core/constant/defaul_button.dart';
import 'package:ecommerce_app/view/widget/details/color_dots.dart';
import 'package:ecommerce_app/view/widget/details/product_description.dart';
import 'package:ecommerce_app/view/widget/details/product_images.dart';
import 'package:ecommerce_app/view/widget/details/top_rounded_container.dart';
import 'package:flutter/material.dart';

import '../../../data/model/product_model.dart';

class Body extends StatelessWidget {
  final Product product;

  const Body({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ProductImages(product: product),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              ProductDescription(
                product: product,
                pressOnSeeMore: () {},
              ),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    ColorDots(product: product),
                    TopRoundedContainer(
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 40,
                          top: 15,
                        ),
                        child: DefaultButton(
                          text: "Add To Cart",
                          press: () {},
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
