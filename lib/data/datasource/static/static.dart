import 'package:ecommerce_app/data/model/onboardin_model.dart';
import 'package:get/get_utils/get_utils.dart';

import '../../../core/constant/images.dart';

List<OnBoardingModel> onBoardingList = [
  OnBoardingModel(
      title: "2".tr, body: "3".tr, image: AppImages.onBoardingOne),
  OnBoardingModel(
      title: "4".tr, body: "5".tr, image: AppImages.onBoardingTwo),
  OnBoardingModel(
      title: "6".tr, body: "7".tr, image: AppImages.onBoardingThree),
  // OnBoardingModel(
  //     title: "Fast Delivery",
  //     body:
  //         "We Have a 100k Product , Choose \n Your Product From Our E-commerce Shop",
  //     image: AppImageAsset.onBoardingImageFour),
];
