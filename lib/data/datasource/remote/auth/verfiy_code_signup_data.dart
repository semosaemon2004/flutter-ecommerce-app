import 'package:ecommerce_app/core/class/crud.dart';
import 'package:ecommerce_app/link_api.dart';

class VerfiyCodeSignupData {
  Crud crud;
  VerfiyCodeSignupData(this.crud);
  Future<dynamic> postdata(String email, String verificationCode) async {
    var response = await crud.postData(AppLink.verifyCodeSignUp, {
      "email": email,
      "verificationCode": verificationCode,
    });
    print("Response type: ${response.runtimeType}");
    return response;
  }
}
