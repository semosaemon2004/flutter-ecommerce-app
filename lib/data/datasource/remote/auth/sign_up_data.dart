import 'package:ecommerce_app/core/class/crud.dart';
import 'package:ecommerce_app/link_api.dart';

class SignupData {
  Crud crud;
  SignupData(this.crud);
  postdata(String username ,String email ,String phoneNumber ,String password ) async {
    var response = await crud.postData(AppLink.signup, {
      "username" : username , 
      "email" : email  , 
      "phoneNumber" : phoneNumber , 
      "password" : password  , 
    });
    print("Response type: ${response.runtimeType}");
    return response.fold((l) => l, (r) => r);
  }
}
