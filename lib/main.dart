import 'package:ecommerce_app/core/locallization/change_local.dart';
import 'package:ecommerce_app/core/services/my_sevices.dart';
import 'package:ecommerce_app/test.dart';
import 'package:ecommerce_app/view/screen/language.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'core/class/crud.dart';
import 'core/locallization/translation.dart';
import 'routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialServices();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    LocaleController controller = Get.put(LocaleController());
    Get.lazyPut(() => Crud());
    return GetMaterialApp(
      translations: MyTranslation(),
      title: 'Flutter Demo',
      locale: controller.language,
      debugShowCheckedModeBanner: false,
      theme: controller.appTheme,
      // home:
      //     const Test(),
      //     const Language(),
      //routes: routes,
      initialBinding: BindingsBuilder(() {
        // This binding will ensure that the LocaleController is created when the app starts
        Get.put(LocaleController());
      }),
      getPages: routes,
    );
  }
}
