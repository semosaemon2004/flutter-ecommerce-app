import 'package:dartz/dartz.dart';
import 'package:ecommerce_app/core/class/status_request.dart';
import 'package:ecommerce_app/core/constant/routes_name.dart';
import 'package:ecommerce_app/data/datasource/remote/auth/verfiy_code_signup_data.dart';
import 'package:get/get.dart';

import '../../core/functions/handing_data_controller.dart';

abstract class VerifyCodeSignUpController extends GetxController {
  checkCode();
  goToSuccessSignUp(String verfiyCodeSignUp);
}

class VerifyCodeSignUpControllerImp extends VerifyCodeSignUpController {
  VerfiyCodeSignupData verfiyCodeSignupData = VerfiyCodeSignupData(Get.find());

  String? email;

  StatusRequest? statusRequest;

  @override
  checkCode() {}

  @override
  goToSuccessSignUp(verfiyCodeSignUp) async {
    statusRequest = StatusRequest.loading;
    update();
    var response =
        await verfiyCodeSignupData.postdata(email!, verfiyCodeSignUp);
    print(
        "Response type: ${response.runtimeType}"); // Print the type of response
    statusRequest = handlingData(response);
    if (statusRequest == StatusRequest.success) {
      if (response is Right) {
        // Handle successful response
        Map<dynamic, dynamic> responseData = response.value;
        print(
            "Response status code: ${responseData['statusCode']}"); // Print the status code
        print(
            "Response body: ${responseData['response']}"); // Print the response body
        if (responseData['message'] == "Email verification successful") {
          Get.offNamed(AppRoute.successSignUp);
        } else {
          Get.defaultDialog(
              title: "Warning", middleText: "Invalid Verification Code");
          statusRequest = StatusRequest.failure;
        }
      } else if (response is Left) {
        // Handle failure response
        // Show an appropriate error message or perform other actions
      }
    } else {
      // Handle other status request cases (loading, failure, etc.)
    }

    update();
  }

  @override
  void onInit() {
    email = Get.arguments['email'];
    super.onInit();
  }
}
