import 'dart:convert';

import 'package:ecommerce_app/core/class/status_request.dart';
import 'package:dartz/dartz.dart';
import 'package:ecommerce_app/core/functions/check_internet.dart';
import 'package:http/http.dart' as http;

class Crud {
  Future<Either<StatusRequest, Map>> postData(String linkurl, Map data) async {
    try {
      if (await checkInternet()) {
        var response = await http.post(Uri.parse(linkurl),
            body: jsonEncode(data),
            headers: {'Content-Type': 'application/json'});
        print("Response status code: ${response.statusCode}");

        if (response.statusCode == 200 || response.statusCode == 201) {
          Map responsebody = jsonDecode(response.body);
          print("Response body: $responsebody");
          return Right(responsebody);
        } else if (response.statusCode == 500) {
          return const Left(StatusRequest.serverfailure);
        } else {
          return const Left(StatusRequest.failure);
        }
      } else {
        return const Left(StatusRequest.offlinefailure);
      }
    } catch (error) {
      print("Error: $error");
      return const Left(StatusRequest.serverException);
    }
  }
}
