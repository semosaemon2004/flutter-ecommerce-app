class AppImages {

  static const String rootImages = "assets/images";
  
  static const String logo            = "$rootImages/logo.PNG";
  static const String onBoardingOne   = "$rootImages/onBoarding01.PNG";
  static const String onBoardingTwo   = "$rootImages/onBoarding02.PNG";
  static const String onBoardingThree = "$rootImages/onBoarding03.PNG";
  static const String profileImage    = "$rootImages/Profile Image.png";

  static const String rootLottie = "assets/lottie";
  static const String loading         = "$rootLottie/loading.json";
  static const String offline         = "$rootLottie/offline.json";
  static const String noData          = "$rootLottie/nodata.json";
  static const String server          = "$rootLottie/server.json";

}