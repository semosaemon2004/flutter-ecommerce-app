import 'package:ecommerce_app/core/constant/colors.dart';
import 'package:ecommerce_app/core/constant/icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'routes_name.dart';
import '../../enums.dart';

class CustomBottomNavBar extends StatelessWidget {
  final MenuState selectedMenu;
  const CustomBottomNavBar({super.key, required this.selectedMenu});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 14),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          ),
        ],
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(40),
          topRight: Radius.circular(40),
        ),
      ),
      child: SafeArea(
        top: false,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: SvgPicture.asset(
                AppIcons.shopIcon,
                color: MenuState.home == selectedMenu
                    ? AppColor.primaryColor
                    : AppColor.inActiveIconColor,
              ),
              onPressed: () => Get.toNamed(AppRoute.home),
            ),
            IconButton(
              icon: SvgPicture.asset(AppIcons.heartIcon),
              onPressed: () {},
            ),
            IconButton(
              icon: SvgPicture.asset(AppIcons.chatBubbleIcon),
              onPressed: () {},
            ),
            IconButton(
              icon: SvgPicture.asset(
                AppIcons.userIcon,
                color: MenuState.profile == selectedMenu
                    ? AppColor.primaryColor
                    : AppColor.inActiveIconColor,
              ),
              onPressed: () => Get.toNamed(AppRoute.profile),
            ),
          ],
        ),
      ),
    );
  }
}
