class AppIcons {
  static const String rootIcons = "assets/icons";

  static const String shopIcon       = "$rootIcons/Shop Icon.svg";
  static const String heartIcon      = "$rootIcons/Heart Icon.svg";
  static const String chatBubbleIcon = "$rootIcons/Chat bubble Icon.svg";
  static const String userIcon       = "$rootIcons/User Icon.svg";
  static const String cameraIcon     = "$rootIcons/Camera Icon.svg";
  static const String bell           = "$rootIcons/Bell.svg";
  static const String settings       = "$rootIcons/Settings.svg";
  static const String questionMark   = "$rootIcons/Question mark.svg";
  static const String logOut         = "$rootIcons/Log out.svg";
  static const String trash          = "$rootIcons/Trash.svg";
  static const String cartIcon       = "$rootIcons/Cart Icon.svg";
  static const String receipt         = "$rootIcons/receipt.svg";
}
