import 'package:ecommerce_app/core/constant/colors.dart';
import 'package:flutter/material.dart';

ThemeData themeEnglish = ThemeData(
  fontFamily: "PlayfairDisplay",
  textTheme: const TextTheme(
    headline1: TextStyle(
      fontStyle: FontStyle.italic,
      fontSize: 25,
      fontWeight: FontWeight.bold,
      color: AppColor.black,
    ),
    headline2: TextStyle(
      fontStyle: FontStyle.italic,
      fontSize: 26,
      fontWeight: FontWeight.bold,
      color: AppColor.black,
    ),
    bodyText1: TextStyle(
      fontSize: 20,
      height: 1.5,
      fontWeight: FontWeight.w400,
      color: AppColor.textTitle,
    ),
    bodyText2: TextStyle(
      fontSize: 17,
      height: 1.5,
      fontWeight: FontWeight.w400,
      color: AppColor.textTitle,
    ),
  ),
  primarySwatch: Colors.blue,
);

ThemeData themeArabic = ThemeData(
  fontFamily: "Cairo",
  textTheme: const TextTheme(
    headline1: TextStyle(
      fontStyle: FontStyle.italic,
      fontSize: 25,
      fontWeight: FontWeight.bold,
      color: AppColor.black,
    ),
    headline2: TextStyle(
      fontStyle: FontStyle.italic,
      fontSize: 26,
      fontWeight: FontWeight.bold,
      color: AppColor.black,
    ),
    bodyText1: TextStyle(
      fontSize: 20,
      height: 1.5,
      fontWeight: FontWeight.w400,
      color: AppColor.textTitle,
    ),
    bodyText2: TextStyle(
      fontSize: 17,
      height: 1.5,
      fontWeight: FontWeight.w400,
      color: AppColor.textTitle,
    ),
  ),
  primarySwatch: Colors.blue,
);
