import 'package:flutter/material.dart';

class AppColor {
  static const Color darkRed = Color(0xffA23962);
  static const Color grey = Color(0xff8e8e8e);
  static const Color textTitle = Color(0xff38424C);
  static const Color black = Color(0xff000000);
  static const Color backgroundColor = Color(0xffF8F9FD);
  static const Color lightRed = Color(0xffF2786D);
  //static const Color primaryColor = Color(0xffF39F5A);
  static const Color primaryColor = Color(0xffB673D6);
  static const Color inActiveIconColor = Color(0xFFB6B6B6);
}
