import 'package:ecommerce_app/core/constant/colors.dart';
import 'package:flutter/material.dart';

import '../../size_confing.dart';

class DefaultButton extends StatelessWidget {
  final String? text;
  final Function? press;

  const DefaultButton({super.key, this.text, this.press});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 56,
      child: TextButton(
        style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          primary: Colors.white,
          backgroundColor: AppColor.primaryColor,
        ),
        onPressed: press as void Function()?,
        child: Text(
          text!,
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
