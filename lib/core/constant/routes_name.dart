class AppRoute {
  // onBoarding
  static const String onBoarding = "/onboarding";
  // Auth
  static const String login = "/login";
  static const String signUp = "/signup";
  static const String forgetPassword = "/forgetpassword";
  static const String verfiyCode = "/verfiycode";
  static const String resetPassword = "/resetpassword";
  static const String successSignUp = "/successsignup";
  static const String successResetPassword = "/successresetpassword";
  static const String checkemail = "/checkemail";
  static const String verfiyCodeSignUp = "/verfiycodesignup";
  // Home
  static const String home = "/home";
  static const String profile = "/profile";
  static const String card = "/card";
  static const String details = "/details";

}
