import 'package:ecommerce_app/view/screen/details/details_screen.dart';
import 'package:ecommerce_app/view/widget/home/product_card.dart';
import 'package:ecommerce_app/view/widget/home/section_title.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'routes_name.dart';
import '../../data/model/product_model.dart';

class PopularProducts extends StatelessWidget {
  const PopularProducts({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SectionTitle(title: "Popular Products", press: () {}),
        ),
        const SizedBox(height: 20),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ...List.generate(
                demoProducts.length,
                (index) {
                  if (demoProducts[index].isPopular) {
                    return ProductCard(
                      product: demoProducts[index],
                      press: () {
                        Get.toNamed(
                          AppRoute.details,
                          arguments: ProductDetailsArguments(
                              product: demoProducts[index]),
                        );
                      },
                    );
                  }

                  return const SizedBox
                      .shrink(); // here by default width and height is 0
                },
              ),
              const SizedBox(width: 20),
            ],
          ),
        )
      ],
    );
  }
}
