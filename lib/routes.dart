import 'package:ecommerce_app/core/middleware/my_middleware.dart';
import 'package:ecommerce_app/view/screen/auth/forgetpassword/forget_password.dart';
import 'package:ecommerce_app/view/screen/auth/login_screen.dart';
import 'package:ecommerce_app/view/screen/auth/forgetpassword/reset_password.dart';
import 'package:ecommerce_app/view/screen/auth/sign_up_screen.dart';
import 'package:ecommerce_app/view/screen/auth/forgetpassword/success_reset_password.dart';
import 'package:ecommerce_app/view/screen/auth/success_sign_up.dart';
import 'package:ecommerce_app/view/screen/auth/forgetpassword/verfiy_code.dart';
import 'package:ecommerce_app/view/screen/auth/verify_code_sign_up.dart';
import 'package:ecommerce_app/view/screen/cart/cart_screen.dart';
import 'package:ecommerce_app/view/screen/details/details_screen.dart';
import 'package:ecommerce_app/view/screen/home/home_screen.dart';
import 'package:ecommerce_app/view/screen/language.dart';
import 'package:ecommerce_app/view/screen/onboardin.dart';
import 'package:ecommerce_app/view/screen/profile/profile_screen.dart';
import 'package:get/get.dart';

import 'core/constant/routes_name.dart';

List<GetPage<dynamic>>? routes = [
  // Language
  GetPage(name: "/", page: () => const Language(), middlewares: [MyMiddleware()]),

  // onBoarding
  GetPage(name: AppRoute.onBoarding, page: () => const OnBoarding()),

  // Auth
  GetPage(name: AppRoute.login, page: () => const LoginScreen()),
  GetPage(name: AppRoute.signUp, page: () => const SignUpScreen()),
  GetPage(name: AppRoute.forgetPassword, page: () => const ForgetPasswordScreen()),
  GetPage(name: AppRoute.verfiyCode, page: () => const VerfiyCodeScreen()),
  GetPage(name: AppRoute.resetPassword, page: () => const ResetPasswordScreen()),
  GetPage(name: AppRoute.successResetPassword, page: () => const SuccessResetPasswordScreen()),
  GetPage(name: AppRoute.verfiyCodeSignUp, page: () => const VerfiyCodeSignUp()),
  GetPage(name: AppRoute.successSignUp, page: () => const SuccessSignUpScreen()),

  // Home
  GetPage(name: AppRoute.home, page: () => const HomeScreen()),
  GetPage(name: AppRoute.profile, page: () => const ProfileScreen()),
  GetPage(name: AppRoute.card, page: () => const CartScreen()),
  GetPage(name: AppRoute.details, page: () => const DetailsScreen()),
];


